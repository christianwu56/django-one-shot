from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from .forms import TodoListForm, TodoItemForm, TodoItemUpdate

# Create your views here.
def show_list(request):
    list = TodoList.objects.all()
    context = {
        "show_list": list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    lists = get_object_or_404(TodoList, id=id)
    context = {"list_object": lists}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST" and TodoListForm:
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    elif TodoListForm:
        form = TodoListForm()
    else:
        form = None
    context = {
        "list_form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    lists = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=lists)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=lists)

    context = {
        "edit": lists,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    list_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.list.id)
    else:
        form = TodoItemForm()
    context = {
        "forms": form,
    }
    return render(request, "todos/create_item.html", context)


def todo_item_update(request):
    if request.method == "POST":
        form = TodoItemUpdate(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.list.id)
    else:
        form = TodoItemUpdate()
    context = {
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)
