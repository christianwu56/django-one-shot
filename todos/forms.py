from django import forms
from .models import TodoList, TodoItem


class TodoListForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class TodoItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = "__all__"


class TodoItemUpdate(forms.ModelForm):
    class Meta:
        models = TodoItem
        fields = "__all__"
